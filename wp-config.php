<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tmrtrnco_dbftlaw');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hIB).,?fnNmzg(3x+I3Eiz{8#vVQM}+gmj]$-}^[.>|}jf{8=vww%OHv^=mh$26-');
define('SECURE_AUTH_KEY',  'O2)X: S_$Ukl<^l H9n8qTgoK;#Ko~0+ao^btxL if^X%]$}-C-d9Jvp%P/Bc#u_');
define('LOGGED_IN_KEY',    'shE4s_Gc_LMGlGs2.*=E(.tu$?TyoS?pu77]|oJ()%s[IV+Zs%y@mZK`MN:Zq&OK');
define('NONCE_KEY',        '|_mA#r<ZQd$9;c|,.ycr$X5VUrn!!F`*qs<T{HaWaQw(C06z9_2y(v/KOwBg]wVd');
define('AUTH_SALT',        '5q^9Ix-VdQoTS4wVJbW?Azl/a%_^cGmAgR|vm o-s`h7GU~At[/ TU&34|-H4&LL');
define('SECURE_AUTH_SALT', ',R/0tCzN5Nl=So}8/DGo$X{dOi1ZJ|pv303A.8>BA?.-4K!c|HY&_q5cx3-*Yq+^');
define('LOGGED_IN_SALT',   '|![QPsg]+*ssv.O][j|C[5n+.1PbW`9BM#Z`k&2AU3G0Tl@m[R26Lflu<D+J[c]+');
define('NONCE_SALT',       ')2I$V+JZc[6<hmT}?6KY@=rWGgNdh,ykwiiAm^)*$N_5PLt0@KsVuW+Wt26+lAQk');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
